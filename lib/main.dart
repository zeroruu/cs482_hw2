import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('This is my FFXIV Character', style: TextStyle(
                    fontWeight: FontWeight.bold,)
                  ),
                ),
                Text('She is a Paladin', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),

          Icon(Icons.turned_in, color: Colors.red[500],),
          Text('48'),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
            _buildButtonColumn(color, Icons.airline_seat_individual_suite, 'Sleep'),
            _buildButtonColumn(color, Icons.brightness_5, 'Sun'),
            _buildButtonColumn(color, Icons.pets, 'Pets'),
          ],
        ),
      );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
      'My FFXIV characters name is Reithy Seraphim and she is a paladin.'
        ' I have been playing since 1.0 of FFXIV beta with a lot of hours. '
        'I usually play tank roles because I find them to be the most fun.'
        ' One of my favorite parts of the game is playing dressup. '
        'I also like running raids with my friends in game.',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
            children:[
              Image.asset(
                'images/ff_char.png',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection
            ]
        )
      )
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label)
  {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }


}